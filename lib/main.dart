import 'package:flutter/material.dart';
//import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {

  void playSound(int soundNumber){
    final player = AudioCache();
    player.play("note$soundNumber.wav");
  }


 Expanded buildKey({Color cor, int soundNumber}){
   return Expanded(
      child: FlatButton(
        color: cor,
        onPressed: (){
          playSound(soundNumber);
        },
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              buildKey(cor: Colors.red, soundNumber: 1),
              buildKey(cor: Colors.orange, soundNumber: 2),
              buildKey(cor: Colors.yellow, soundNumber: 3),
              buildKey(cor: Colors.teal, soundNumber: 4),
              buildKey(cor: Colors.green, soundNumber: 5),
              buildKey(cor: Colors.blue, soundNumber: 6),
              buildKey(cor: Colors.purple, soundNumber: 7),
            ],
          ),
        ),
      ),
    );
  }
}